//
//  AppDelegate.swift
//  FloatingPanelTabController
//
//  Created by Yuya Oka on 2021/10/31.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = .init(frame: UIScreen.main.bounds)
        window?.rootViewController = ContainerViewController()
        window?.makeKeyAndVisible()
        return true
    }
}
