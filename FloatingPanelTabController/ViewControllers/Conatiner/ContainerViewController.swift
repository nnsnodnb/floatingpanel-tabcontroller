//
//  ContainerViewController.swift
//  FloatingPanelTabController
//
//  Created by Yuya Oka on 2021/10/31.
//

import FloatingPanel
import SnapKit
import UIKit

final class ContainerViewController: UIViewController {

    private let customTabBarController: CustomTabBarController = .init()

    private lazy var floatingPanelController: FloatingPanelController = .init(delegate: self)

    override func viewDidLoad() {
        super.viewDidLoad()

        addChild(customTabBarController)
        view.addSubview(customTabBarController.view)
        customTabBarController.view.snp.makeConstraints { $0.edges.equalToSuperview() }
        customTabBarController.didMove(toParent: self)
        customTabBarController.customDelegate = self

        floatingPanelController.addPanel(toParent: self)
        floatingPanelController.layout = CustomFloatingPanelLayout()
        floatingPanelController.invalidateLayout()
        floatingPanelController.hide()
    }
}

// MARK: - CustomTabBarViewControllerDelegate
extension ContainerViewController: CustomTabBarViewControllerDelegate {

    func customTabBarViewControllerShouldOpenSemiModal() {
        floatingPanelController.move(to: .half, animated: true)
    }
}

// MARK: - FloatingPanelControllerDelegate
extension ContainerViewController: FloatingPanelControllerDelegate {}

private final class CustomFloatingPanelLayout: FloatingPanelLayout {

    // MARK: - Properties
    let position: FloatingPanelPosition = .bottom
    let initialState: FloatingPanelState = .hidden

    var anchors: [FloatingPanelState: FloatingPanelLayoutAnchoring] {
        return [
            .half: FloatingPanelLayoutAnchor(absoluteInset: UIScreen.main.bounds.height / 4,
                                             edge: .bottom,
                                             referenceGuide: .safeArea),
            .hidden: FloatingPanelLayoutAnchor(fractionalInset: 0,
                                               edge: .bottom,
                                               referenceGuide: .superview)
        ]
    }

    func backdropAlpha(for state: FloatingPanelState) -> CGFloat {
        return state == .half ? 0.75 : 0
    }
}
