//
//  CustomTabBarController.swift
//  FloatingPanelTabController
//
//  Created by Yuya Oka on 2021/10/31.
//

import FloatingPanel
import UIKit

protocol CustomTabBarViewControllerDelegate: AnyObject {

    func customTabBarViewControllerShouldOpenSemiModal()
}

final class CustomTabBarController: UITabBarController {

    weak var customDelegate: CustomTabBarViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        let appearance = UITabBarAppearance()
        appearance.backgroundColor = .white
        appearance.shadowImage = .init()
        tabBar.standardAppearance = appearance
        if #available(iOS 15.0, *) {
            tabBar.scrollEdgeAppearance = appearance
        }
        tabBar.backgroundImage = .init()

        let viewController = FirstViewController()
        viewController.delegate = self
        setViewControllers([viewController], animated: false)
    }
}

// MARK: - FirstViewControllerDelegate
extension CustomTabBarController: FirstViewControllerDelegate {

    func firstViewControllerDidTapButton() {
        customDelegate?.customTabBarViewControllerShouldOpenSemiModal()
    }
}
