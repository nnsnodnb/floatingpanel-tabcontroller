//
//  FirstViewController.swift
//  FloatingPanelTabController
//
//  Created by Yuya Oka on 2021/10/31.
//

import UIKit

protocol FirstViewControllerDelegate: AnyObject {

    func firstViewControllerDidTapButton()
}

final class FirstViewController: UIViewController {

    weak var delegate: FirstViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func onTapButton(_ sender: Any) {
        delegate?.firstViewControllerDidTapButton()
    }
}
